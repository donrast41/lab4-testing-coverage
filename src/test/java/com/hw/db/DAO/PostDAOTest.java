package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import java.sql.Timestamp;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;

class PostDAOTest {

    private Post tstPst;
    private Post updtstPst;
    @org.junit.jupiter.api.Test
    void pstTstone() {
        tstPst = new Post("Donrast",new Timestamp(0),"forum","message",1,8,true);
        updtstPst= new Post("Donr",new Timestamp(1),"forum","new message",1,8,true);
        JdbcTemplate copyJD = mock(JdbcTemplate.class);
        PostDAO pstDAO = new PostDAO(copyJD);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Mockito.when(copyJD.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(updtstPst);
        PostDAO.setPost(1, tstPst);
        Mockito.verify(copyJD).update("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;", tstPst.getAuthor(), tstPst.getMessage(), tstPst.getCreated(), 1);
    }
    @org.junit.jupiter.api.Test
    void pstTsttwo() {
        tstPst = new Post("Donrast",null,"forum","message",1,8,true);
        updtstPst= new Post("Donr",null,"forum","new message",1,8,true);
        JdbcTemplate copyJD = mock(JdbcTemplate.class);
        PostDAO pstDAO = new PostDAO(copyJD);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Mockito.when(copyJD.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(updtstPst);
        PostDAO.setPost(1, tstPst);
        Mockito.verify(copyJD).update("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;", tstPst.getAuthor(), tstPst.getMessage(), 1);
    }
    @org.junit.jupiter.api.Test
    void pstTstthree() {
        tstPst = new Post("Donrast",new Timestamp(0),"forum",null,1,8,true);
        updtstPst= new Post("Donr",new Timestamp(1),"forum",null,1,8,true);
        JdbcTemplate copyJD = mock(JdbcTemplate.class);
        PostDAO pstDAO = new PostDAO(copyJD);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Mockito.when(copyJD.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(updtstPst);
        PostDAO.setPost(1, tstPst);
        Mockito.verify(copyJD).update("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;", tstPst.getAuthor(), tstPst.getCreated(), 1);
    }
    @org.junit.jupiter.api.Test
    void pstTstfour() {
        tstPst = new Post(null,new Timestamp(0),"forum","message",1,8,true);
        updtstPst= new Post(null,new Timestamp(1),"forum","new message",1,8,true);
        JdbcTemplate copyJD = mock(JdbcTemplate.class);
        PostDAO pstDAO = new PostDAO(copyJD);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Mockito.when(copyJD.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(updtstPst);
        PostDAO.setPost(1, tstPst);
        Mockito.verify(copyJD).update("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",  tstPst.getMessage(), tstPst.getCreated(), 1);
    }
    @org.junit.jupiter.api.Test
    void pstTstfive() {
        tstPst = new Post("Donrast",new Timestamp(0),"forum","message",1,8,true);
        updtstPst= new Post("Donrast",new Timestamp(0),"forum","new message",1,8,true);
        JdbcTemplate copyJD = mock(JdbcTemplate.class);
        PostDAO pstDAO = new PostDAO(copyJD);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Mockito.when(copyJD.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(updtstPst);
        PostDAO.setPost(1, tstPst);
        Mockito.verify(copyJD).update("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;",  tstPst.getMessage(), 1);
    }
    @org.junit.jupiter.api.Test
    void pstTstsix() {
    tstPst = new Post("Donrast",new Timestamp(0),"forum","message",1,8,true);
    updtstPst= new Post("Donrast",new Timestamp(1),"forum","message",1,8,true);
        JdbcTemplate copyJD = mock(JdbcTemplate.class);
        PostDAO pstDAO = new PostDAO(copyJD);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Mockito.when(copyJD.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(updtstPst);
        PostDAO.setPost(1, tstPst);
        Mockito.verify(copyJD).update("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",  tstPst.getCreated(),  1);
    }
    @org.junit.jupiter.api.Test
    void pstTstseven() {
    tstPst = new Post("Donrast",new Timestamp(0),"forum","message",1,8,true);
    updtstPst= new Post("Donr",new Timestamp(0),"forum","message",1,8,true);
        JdbcTemplate copyJD = mock(JdbcTemplate.class);
        PostDAO pstDAO = new PostDAO(copyJD);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Mockito.when(copyJD.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(updtstPst);
        PostDAO.setPost(1, tstPst);
        Mockito.verify(copyJD).update("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;",  tstPst.getAuthor(), 1);
    }
    @org.junit.jupiter.api.Test
    void pstTsteight() {
    tstPst = new Post("Donrast",new Timestamp(0),"forum","message",1,8,true);
    updtstPst= new Post("Donrast",new Timestamp(0),"forum","message",1,8,true);
        JdbcTemplate copyJD = mock(JdbcTemplate.class);
        PostDAO pstDAO = new PostDAO(copyJD);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Mockito.when(copyJD.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(1))).thenReturn(updtstPst);
        PostDAO.setPost(1, tstPst);
        Mockito.verify(copyJD, Mockito.never()).update(Mockito.anyString(), Mockito.anyCollection());
    }
}
