package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class UserDAOTest {
    private User tstUsr;
    @Test
    void changeTest1() {
        tstUsr = new User("Ezio","email.com",null,null
        );
        try (MockedStatic<ThreadDAO> copyThrd = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate copyJD = mock(JdbcTemplate.class);
            UserDAO usrDa = new UserDAO(copyJD);
            UserDAO.Change(tstUsr);
            Mockito.verify(copyJD).update("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;",tstUsr.getEmail(),  tstUsr.getNickname());
        }
    }
    @Test
    void changeTest2() {
        tstUsr = new User("Ezio",null,"Donrast",null);
        try (MockedStatic<ThreadDAO> copyThrd = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate copyJD = mock(JdbcTemplate.class);
            UserDAO usrDa = new UserDAO(copyJD);
            UserDAO.Change(tstUsr);
            Mockito.verify(copyJD).update("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;",tstUsr.getFullname(),  tstUsr.getNickname());
        }
    }
    @Test
    void changeTest3() {
        tstUsr = new User("Ezio",null,null,"Am writing test cases");
        try (MockedStatic<ThreadDAO> copyThrd = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate copyJD = mock(JdbcTemplate.class);
            UserDAO usrDa = new UserDAO(copyJD);
            UserDAO.Change(tstUsr);
            Mockito.verify(copyJD).update("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;",tstUsr.getAbout(),  tstUsr.getNickname());
        }
    }
    @Test
    void changeTest4() {
        tstUsr = new User("Ezio",null,null,null);
        try (MockedStatic<ThreadDAO> copyThrd = Mockito.mockStatic(ThreadDAO.class)) {
            JdbcTemplate copyJD = mock(JdbcTemplate.class);
            UserDAO usrDa = new UserDAO(copyJD);
            UserDAO.Change(tstUsr);
            Mockito.verify(copyJD, Mockito.never()).update(Mockito.anyString(), Mockito.anyCollection());
        }
    }
}
