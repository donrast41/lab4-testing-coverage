package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import java.util.Collections;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class ThreadDAOTest {
    private List<Object> voidArr;
    @org.junit.jupiter.api.BeforeEach
    @org.junit.jupiter.api.DisplayName("testing")
    void createStubs() {
        voidArr = Collections.emptyList();
    }
    @Test
    void treeSort1() {
        JdbcTemplate copyJD = mock(JdbcTemplate.class);
        ThreadDAO frm = new ThreadDAO(copyJD);
        assertEquals(voidArr, ThreadDAO.treeSort(1, 1,1, true));
    }
    @Test
    void treeSort2() {
        JdbcTemplate copyJD = mock(JdbcTemplate.class);
        ThreadDAO frm = new ThreadDAO(copyJD);
        assertEquals(voidArr, ThreadDAO.treeSort(1, 1,1, false));
    }
}
